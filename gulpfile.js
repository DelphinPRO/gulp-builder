// include gulp
const gulp = require('gulp');
const path = require('path'); // Стандартный node модуль для работы с путями

// include plugins
const del          = require('del');
const sass         = require('gulp-sass');
const concat       = require('gulp-concat');
const uglify       = require('gulp-uglify');
const autoprefixer = require('gulp-autoprefixer');
const browserSync  = require('browser-sync');
const css_nano     = require('gulp-cssnano');
const plumber      = require('gulp-plumber');
const mainBower    = require('gulp-main-bower-files');

const root_path = 'templates/sweetsugar';

// Настройки для scss
let scss = {
    source      : 'asset/sass/**/*.scss',
    dest        : 'css/',
    first       : 'all.css',       // Первый в склейке
    output      : 'build.min.css', // Выходной общий файл
    options     : {
        outputStyle: 'expanded'
    },
    autoprefixer: {
        browsers: ['last 15 versions'],
        cascade : false,
    },
};

// compile scss
let scss_paths_source = [
    path.join(root_path, scss.source), // компилируемые файлы
    '!_*.scss' // исключения, файлы, начинающиеся с подчеркивания
];

gulp.task('sass', function () {
    return gulp.src(scss_paths_source)
        .pipe(plumber())
        .pipe(sass(scss.options))
        .pipe(autoprefixer(scss.autoprefixer))
        .pipe(gulp.dest(path.join(root_path, scss.dest)));
});

// concat and minify css
gulp.task('concat:css', ['sass'], function () {
    return gulp.src([
        path.join(root_path, scss.dest, scss.first), // Первый в склейке
        path.join(root_path, scss.dest,'*.css') // Все остальные
    ])
        .pipe(plumber())
        .pipe(concat(scss.output)) // Название выходного файла
        .pipe(css_nano())
        .pipe(gulp.dest(path.join(root_path, scss.dest)))
        .pipe(browserSync.reload({
            stream: true
        }));
});

// Склейка сторонних библиотек
gulp.task('bower:concat', function () {
    return gulp.src('./bower.json')
        .pipe(mainBower())
        .pipe(concat('vendors.js'))
        .pipe(gulp.dest('js/'));
});

// concatenate and minify js
gulp.task('scripts', ['bower:concat'], function () {
    return gulp.src([
        'js/vendors.js',
        path.join(root_path, 'asset/js/*.js')
    ])
        .pipe(plumber())
        .pipe(concat('build.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('js/'))
        .pipe(browserSync.reload({
            stream: true
        }));
});

// browserSync task
gulp.task('browserSync', ['concat:css', 'scripts'], function () {
    browserSync.init({
        proxy: "http://sweetsugar.loc/"
    });
});

// start watchers
gulp.task('watch', ['browserSync'], function () {
    gulp.watch(path.join(root_path, scss.source), ['scss']);
    gulp.watch(path.join(root_path, 'asset/**/*.js'), ['scripts']);
    gulp.watch(path.join(root_path, '**/*.php'), browserSync.reload);
    // other watchers
});

// Очистка сборки
gulp.task('clean', function (cb) {
    del([
        path.join(root_path, scss.dest),
        'js/'
    ]).then(function (paths) {
        paths.forEach(function (path) {
            console.log(`Remove path: ${path}`);
        });
        cb();
    })
});
